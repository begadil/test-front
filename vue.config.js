module.exports = {
  transpileDependencies: ["vuetify"],
  chainWebpack: config => {
    const svgRule = config.module.rule("svg");

    svgRule.uses.clear();

    svgRule
      .use("babel-loader")
      .loader("babel-loader")
      .end()
      .use("vue-svg-loader")
      .loader("vue-svg-loader");
  },
  configureWebpack: config => {
    const eslintRule = config.module.rules.find(v => {
      return v.enforce === 'pre' &&
        v.test.test(".js") &&
        v.test.test(".jsx") &&
        v.test.test(".vue") &&
        v.test.test(".tsx")
    })
    if (eslintRule) {
      eslintRule.exclude.push(/sheetUI4.2/);
    }
    const babelRule = config.module.rules.find(v => {
      return (
        v.test.test(".js") &&
        v.test.test(".jsx") &&
        !v.test.test(".vue") &&
        !v.enforce
      );
    });
    if (babelRule) {
      babelRule.exclude.push(/sheetUI4.2/);
    }
  }
};
