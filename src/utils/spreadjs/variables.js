export const colors = {
  auto: ["", "#000000", "#ff0000", "#008000", "#0000ff"],
  palette: [
    "#fff",
    "#000",
    "#e7e6e6",
    "#475468",
    "#689ad0",
    "#df8244",
    "#a5a5a5",
    "#f6c143",
    "#4d72be",
    "#7eab55",
    "#f2f2f2",
    "#808080",
    "#cfcdcd",
    "#d6dce3",
    "#e0eaf5",
    "#f7e5d7",
    "#ededed",
    "#fdf2d0",
    "#dae2f1",
    "#e4eedb",
    "#d9d9d9",
    "#595959",
    "#adaaaa",
    "#aeb9c8",
    "#c1d6ec",
    "#f0cbb0",
    "#dbdbdb",
    "#fbe5a3",
    "#b7c6e4",
    "#cadfb7",
    "#bfbfbf",
    "#404040",
    "#737070",
    "#8796ae",
    "#a3c1e2",
    "#eab28a",
    "#c9c9c9",
    "#fad978",
    "#93aad7",
    "#b0ce94",
    "#a6a6a6",
    "#262626",
    "#3a3838",
    "#343e4e",
    "#3f74b0",
    "#b75f29",
    "#7b7b7b",
    "#b8902f",
    "#365491",
    "#5d7f3f",
    "#7f7f7f",
    "#0d0d0d",
    "#161616",
    "#222933",
    "#2a4d75",
    "#7a3f1a",
    "#525252",
    "#7b601c",
    "#233760",
    "#3e552a"
  ],
  standard: [
    "#b12318",
    "#eb3323",
    "#f6c143",
    "#fffe54",
    "#a0cd63",
    "#4ead5b",
    "#4baeea",
    "#2b70ba",
    "#05215c",
    "#67379a"
  ]
};
export const boldValues = {
  bold: 1,
  500: 1,
  600: 1,
  700: 1,
  800: 1,
  900: 1
};
export const fonts = [
  "Arial",
  "Arial Black",
  "Arial Rounded MT",
  "Calibri",
  "Calibri",
  "Calibri Light",
  "Cambria",
  "Century Gothic",
  "Comic Sans MS",
  "Consolas",
  "Courier",
  "Courier New",
  "Garamond",
  "Georgia",
  "Helvetica",
  "Impact",
  "Lucida Console",
  "Nueva Roman",
  "Sans-Serif",
  "Segoe UI",
  "Segoe UI Semibold",
  "Segoe UI Symbol",
  "Segoe UI, Light",
  "Serif",
  "Times New Roman",
  "Trebuchet MS",
  "Verdana"
];
export const fontSizes = [8, 9, 10, 12, 14, 16, 18, 20, 24, 28, 32];

const numberFormats = {
  General: "Общий",
  '# ##0_ ;[Red]-# ##0_\ ': 'Числовой', // eslint-disable-line
  "0,00%": "Процент",
  "dd.MM.yyyy": "Дата",
  "mmm-yy": "Месяц год",
  "dd.MM.yyyy dddd": "Date day",
  "0,0E+00": "Экспоненциальный",
  '_-* # ##0,00 ₽_-;-* # ##0,00 ₽_-;_-* "-"?? ₽_-;_-@_-': "Финансовый ₽",
  '_-[$$-409]* # ##0,00_ ;_-[$$-409]* -# ##0,00\ ;_-[$$-409]* "-"??_ ;_-@_ ': 'Финансовый $', // eslint-disable-line
  '_-[$€-2] * # ##0,00_-;-[$€-2] * # ##0,00_-;_-[$€-2] * "-"??_-;_-@_-':
    "Финансовый €",
  '_-* # ##0,00\ [$₸-43F]_-;-* # ##0,00\ [$₸-43F]_-;_-* "-"??\ [$₸-43F]_-;_-@_-': 'Финансовый ₸', // eslint-disable-line
  '_ [$¥-478]* # ##0,00_ ;_ [$¥-478]* -# ##0,00_ ;_ [$¥-478]* "-"??_ ;_ @_ ':
    "Финансовый ¥",
  '[$$-409]# ##0,00_ ;-[$$-409]# ##0,00\ ': 'Денежный $', // eslint-disable-line
  "[$€-2] # ##0,00;-[$€-2] # ##0,00": "Денежный €",
  '# ##0,00\ [$₽-419];-# ##0,00\ [$₽-419]': 'Денежный ₽', // eslint-disable-line
  "[$¥-478]# ##0,00;[$¥-478]-# ##0,00": "Денежный ¥",
  '# ##0,00\ [$₸-43F];-# ##0,00\ [$₸-43F]': 'Денежный ₸' // eslint-disable-line
};

export const getFormatter = formatter => {
  if (!formatter) {
    return "General";
  }
  let result = "";
  if (typeof formatter === "object") {
    result = formatter.typeName;
  } else if (typeof formatter === "string") {
    result = formatter;
  }
  result = result.toLowerCase().replace(/[\\]/g, "");
  result = Object.keys(numberFormats).find(v => result === v.toLowerCase());
  return result || "General";
};

export const ribbonNumberFormats = Object.entries(numberFormats).map(
  ([k, v]) => {
    return {
      name: k,
      label: v,
      value: k
    };
  }
);

export const ribbonFontItems = fonts.map(font => {
  return {
    name: font,
    label: font,
    value: font
  };
});

export const ribbonFontSizes = fontSizes.map(v => {
  return {
    name: v + "px",
    label: v,
    value: v + "px"
  };
});
