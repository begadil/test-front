import {
  colors,
  ribbonNumberFormats,
  ribbonFontSizes,
  ribbonFontItems
} from "@/utils/spreadjs/variables";

export default {
  quickLaunchToolbar: [
    {
      name: "save", // command name
      hint: "Сохранить (Ctrl+S)", // optional button tooltip
      icon: "mdi-content-save" // button icon
    },
    {
      name: "cut",
      hint: "Вырезать (Ctrl+X)",
      icon: "mdi-content-cut"
    },
    {
      name: "copy",
      hint: "Копировать (Ctrl+C)",
      icon: "mdi-content-copy"
    },
    {
      name: "paste",
      hint: "Вставить (Ctrl+V)",
      icon: "mdi-content-paste"
    },
    {
      name: "undo",
      hint: "Назад (Ctrl+Z)",
      icon: "mdi-undo"
    },
    {
      name: "redo",
      hint: "Вперед (Ctrl+X)",
      icon: "mdi-redo"
    }
  ],
  tabs: [
    {
      label: "Главная",
      ribbons: [
        {
          label: "Шрифт",
          name: "font",
          width: "300px",
          tools: [
            {
              type: "dropdown",
              name: "dropdown:font-family",
              width: "150px",
              items: ribbonFontItems
            },
            {
              type: "dropdown",
              name: "dropdown:font-size",
              width: "70px", // width of the dropdown in pixels or percentage. if not set, width will be automatic
              items: ribbonFontSizes
            },
            {
              type: "buttons",
              size: "small",
              name: "buttons:font-sizing",
              commands: [
                {
                  name: "font-grow",
                  hint: "Grow Font",
                  icon: "mdi-plus"
                },
                {
                  name: "font-shrink",
                  hint: "Shrink Font",
                  icon: "mdi-minus"
                }
                // {
                //   name: "format-clear",
                //   hint: "Clear formatting",
                //   icon: "mdi-format-clear"
                // }
              ]
            },
            {
              type: "break"
            },
            {
              type: "toggle-buttons",
              name: "toggle-buttons:bold-italic-underline-strikethrough",
              size: "small",
              commands: [
                {
                  name: "bold",
                  hint: "Жирный (Ctrl+B)",
                  icon: "format_bold"
                },
                {
                  name: "italic",
                  hint: "Курсив (Ctrl+I)", // optional
                  icon: "format_italic"
                },
                {
                  name: "underline",
                  hint: "Подчеркнутый (Ctrl+U)", // optional
                  icon: "format_underline"
                },
                {
                  name: "strikethrough",
                  hint: "Зачеркнутый", // optional
                  icon: "strikethrough_s"
                }
              ]
            },
            {
              type: "separator"
            },
            {
              type: "color-picker",
              name: "color-picker:font-color",
              size: "small",
              hint: "Установить цвет шрифта", // optional
              icon: "font_download",
              labels: {
                standard: "Стандартные цвета",
                palette: "Палитра",
                auto: "Авто"
              },
              colors: colors
            },
            {
              type: "color-picker",
              name: "color-picker:highlight-color",
              hint: "Выбрать фоновый цвет",
              icon: "mdi-palette",
              size: "small",
              labels: {
                standard: "Стандартные цвета",
                palette: "Палитра",
                auto: "Авто"
              },
              colors: colors
            },
            {
              type: "menu",
              name: "menu:border-style",
              size: "small",
              hint: "Тип границ",
              dropDownCssClasses: [], // additional optional CSS classes to apply to the menu dropdown
              activator: {
                icon: "border_style"
              },
              commands: [
                {
                  name: "none",
                  icon: "border_clear",
                  label: "Нет"
                },
                {
                  name: "all",
                  icon: "border_all",
                  label: "Все"
                },
                {
                  name: "left",
                  icon: "border_left",
                  label: "Левая"
                },
                {
                  name: "top",
                  icon: "border_top",
                  label: "Верхняя"
                },
                {
                  name: "right",
                  icon: "border_right",
                  label: "Правая"
                },
                {
                  name: "bottom",
                  icon: "border_bottom",
                  label: "Нижняя"
                },
                {
                  name: "outline",
                  icon: "border_outer",
                  label: "Контур"
                },
                {
                  name: "inside",
                  icon: "border_inner",
                  label: "Внутри"
                },
                {
                  name: "innerHorizontal",
                  icon: "border_horizontal",
                  label: "Горизонтальная"
                },
                {
                  name: "innerVertical",
                  icon: "border_vertical",
                  label: "Вертикальная"
                }
              ]
            }
          ]
        },
        {
          label: "Числа",
          name: "numbers",
          width: "110px",
          tools: [
            {
              type: "dropdown",
              name: "dropdown:number-format",
              width: "110px", // width of the dropdown in pixels or percentage. if not set, width will be automatic
              items: ribbonNumberFormats
            },
            {
              type: "buttons",
              size: "small",
              name: "buttons:number-format",
              commands: [
                {
                  name: "percentage",
                  hint: "Процент", // optional
                  icon: "mdi-percent"
                },
                {
                  name: "decimal",
                  hint: "Десятичный", // optional
                  // label: 'Shrink Font', // optional
                  icon: "mdi-decimal-comma"
                },
                {
                  name: "increase-decimal",
                  hint: "Увеличить число десятичных знаков", // optional
                  // label: 'Clear formatting', // optional
                  icon: "mdi-decimal-comma-increase"
                },
                {
                  name: "decrease-decimal",
                  hint: "Уменьшить число десятичных знаков", // optional
                  // label: 'Clear formatting', // optional
                  icon: "mdi-decimal-comma-decrease"
                }
              ]
            }
          ]
        },
        {
          label: "Сортировка и фильтрация",
          name: "sort-filter",
          width: "60px", // optional width in pixels or percentage, if not set ribbon width will be automatic
          tools: [
            {
              type: "buttons",
              size: "small",
              name: "buttons:filter",
              items: "break", // display the buttons list vertically
              commands: [
                {
                  name: "filter",
                  hint: "Фильтр", // optional
                  // label: "Filter", // optional
                  icon: "mdi-filter"
                },
                {
                  name: "remove-filter",
                  hint: "Удалить фильтр", // optional
                  label: "", // optional
                  icon: "mdi-filter-remove"
                }
              ]
            },
            {
              type: "buttons",
              size: "small",
              name: "buttons:sort",
              items: "break", // display the buttons list vertically
              commands: [
                {
                  name: "ascending-sort",
                  hint: "Сортировать по возрастанию", // optional
                  label: "", // optional
                  icon: "mdi-sort-ascending"
                },
                {
                  name: "descending-sort",
                  hint: "Сортировать по убыванию", // optional
                  label: "", // optional
                  icon: "mdi-sort-descending"
                }
              ]
            }
          ]
        },
        {
          label: "Границы и группы",
          name: "outlines",
          width: "100px", // optional width in pixels or percentage, if not set ribbon width will be automatic
          tools: [
            {
              type: "buttons",
              size: "small",
              name: "buttons:outlines",
              commands: [
                {
                  name: "group-column",
                  hint: "Группировать столбец",
                  icon: "mdi-table-column-width"
                },
                {
                  name: "group-row",
                  hint: "Группировать строку",
                  icon: "mdi-table-row-height"
                },
                {
                  name: "expand",
                  hint: "Развернуть",
                  icon: "mdi-arrow-expand-vertical"
                },
                {
                  name: "collapse",
                  hint: "Свернуть",
                  icon: "mdi-arrow-collapse-vertical"
                },
                {
                  name: "show",
                  hint: "Показать группы",
                  icon: "mdi-eye"
                },
                {
                  name: "hide",
                  hint: "Спрятать группы",
                  icon: "mdi-eye-off"
                },
                {
                  name: "ungroup",
                  hint: "Разгруппировать",
                  icon: "mdi-window-close"
                }
              ]
            },
            {
              type: "menu", // define type of the tool
              name: "menu:freeze-pane", // menu name
              size: "small", // tool size - "small" or "large"
              hint: "Закрепить", // optional menu tooltip

              dropDownCssClasses: ["custom-css-class-1", "custom-css-class-2"], // optional array of custom CSS classes to apply to the menu dropdown

              // mdi-dock-left
              // mdi-view-compact
              activator: {
                label: "Закрепить", // menu main item label
                icon: "mdi-dock-left" // menu main item icon
              },
              commands: [
                {
                  name: "freeze-pane",
                  label: "По выделению",
                  icon: "mdi-dock-left"
                },
                {
                  name: "freeze-top-row",
                  label: "Первая строка",
                  icon: "mdi-dock-left"
                },
                {
                  name: "freeze-first-column",
                  label: "Первый столбец",
                  icon: "mdi-dock-left"
                },
                {
                  name: "freeze-bottom-row",
                  label: "Последняя строка",
                  icon: "mdi-dock-left"
                },
                {
                  name: "freeze-last-column",
                  label: "Последний столбец",
                  icon: "mdi-dock-left"
                },
                {
                  name: "freeze-clear",
                  label: "Очистить",
                  icon: "mdi-dock-left"
                }
              ]
            }
          ]
        },
        {
          label: "Абзац",
          name: "paragraph",
          width: "135px",
          tools: [
            {
              type: "exclusive-buttons",
              name: "exclusive-buttons:format-halign",
              size: "small",
              commands: [
                {
                  name: "left",
                  hint: "Выровнять по левому краю",
                  icon: "mdi-format-align-left"
                },
                {
                  name: "center",
                  hint: "Выровнять по центру",
                  icon: "mdi-format-align-center"
                },
                {
                  name: "right",
                  hint: "Выровнять по правому краю",
                  icon: "mdi-format-align-right"
                }
              ]
            },
            {
              type: "buttons",
              size: "small",
              name: "buttons:indent",
              commands: [
                {
                  name: "outdent",
                  hint: "Уменьшить отступ",
                  icon: "mdi-format-indent-decrease"
                },
                {
                  name: "indent",
                  hint: "Увеличить отступ",
                  icon: "mdi-format-indent-increase"
                }
              ]
            },
            {
              type: "break"
            },
            {
              type: "exclusive-buttons",
              name: "exclusive-buttons:format-valign",
              size: "small",
              commands: [
                {
                  name: "bottom",
                  hint: "Выровнять по нижнему краю",
                  icon: "mdi-format-align-bottom"
                },
                {
                  name: "center",
                  hint: "Выровнять по середине",
                  icon: "mdi-format-align-middle"
                },
                {
                  name: "top",
                  hint: "Выровнять по верхнему краю",
                  icon: "mdi-format-align-top"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
};
