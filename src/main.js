import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";

import "./scss/main.scss";

Vue.use(Loading);

Vue.config.productionTip = false;
Vue.prototype.$hostname =
  process.env.NODE_ENV === "production"
    ? "http://95.217.4.21"
    : "http://localhost:8000";

Vue.prototype.$loadingParameters = {
  canCancel: false,
  color: "#e7e7e7",
  loader: "dots",
  width: 128,
  height: 128,
  backgroundColor: "#000",
  opacity: 0.7,
  zIndex: 10000
};

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount("#app");
