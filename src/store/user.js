import axios from "axios";

export default {
  state: {
    user: null
  },
  mutations: {
    setUser(state, payload) {
      state.user = payload;
    }
  },
  actions: {
    async loginUser({ commit }, { login, password }) {
      const usr = {
        username: login,
        password: password
      };

      await axios
        .post("http://178.88.161.73:8007/api/login", usr)
        .then(response => {
          console.log(response.data);
          console.log(response.data.role);
          let role = response.data.role;
          let fio = response.data.fio;
          commit("setUser", role);
          localStorage.role = role;
          window.$cookies.set("role", role);
          window.$cookies.set("fio", fio);
        })
        .catch(error => {
          console.log("error!!!");
          console.log(error);
          throw error;
        });
    }
  },
  getters: {
    user(state) {
      return state.user;
    }
  }
};
