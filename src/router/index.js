import Vue from "vue";
import VueRouter from "vue-router";
import auth from "./auth";

Vue.use(VueRouter);

function configRoutes() {
  return [
    {
      path: "/",
      component: () => import("../layout/MainPanel.vue"),
      beforeEnter: auth,
      children: [
        {
          path: "",
          name: "Home",
          component: () => import("../views/Home.vue")
        }
      ]
    },
    {
      path: "/login",
      name: "Авторизация",
      component: () => import("../views/Login.vue")
    }
  ];
}

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  scrollBehavior: () => ({
    y: 0
  }),
  routes: configRoutes()
});

export default router;
