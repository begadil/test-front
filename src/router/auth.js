export default function(from, to, next) {
  if (localStorage.role === "new") {
    next();
  } else {
    next("/login");
  }
}
